#ifndef TKRTSARANGESELECTOR_H
#define TKRTSARANGESELECTOR_H

/** @file
 *  @class 
 *  @brief Класс предназначен для поиска опорных дальностей из очереди TMainQueue на основе входных параметров.
 */

#include "TMainQueue.h"
#include "eml.h"
#include "functions.h"

#define SIZEOF_EML32_F 4
#define SIZEOF_EML32_FC 8

class TKrtSaRangeSelector{
	private:
		int M_ps;
		int MAX_Nni;
		int MAX_Ndd;

		int analisRangeCount;

		bool* analisRange;

		eml_32f Sr_m;

		eml_32f* SrSum;
		eml_32f* Sr;

		eml_32fc*** s_;
		eml_32fc*** s_d;
	public:

		/**
		* @brief: Конструктор класса выделяет необходимую память на основе входных параметров
		*
		* @param: int MAX_Ndd - максимальное количество отсчетов по дальности
		*       : int MAX_Nni - максимальное количество отсчетов по азимуту  
		*       : int M_ps - количество литер
		*/
		TKrtSaRangeSelector(int MAX_Ndd, int MAX_Nni, int M_ps);
		virtual ~TKrtSaRangeSelector();
		
		void reset();

		/**
		 * @brief: Производит выборку по дальностям и записывает результаты в s_ и s_d
		 *
		 * @param: signalItem* start - указатель на первый элемент очереди
		 *       : int Ndd - чистро стробов дальности с разрешением от M_ps литер
		 *       : int Nni - количество сигналов по азимуту для одной литеры
		 *       : int Ndd_result - ограничение для выборки по азимуту (не более Ndd_result)
		 *
		 * @return: void 
		 */
		void  execute(signalItem* start, int Ndd, int Nni, int Ndd_result);

		/**
		 * @brief: Возвращает количество опорных дальностей
		 *
		 * @return: int - количество опорных дальностей
		 */
		int getAnalisRangeCount();

		/**
		 * @brief: Возвращает указатель на массив подходящих дальностей 
		 *
		 * @return: bool - указатель на массив подходящих дальностей
		 */
		bool* getAnalisRange();

		/**
		 * @brief: Возвращает указатель на трехмерный массив опорных дальностей для суммарного канала. Первое измерение - по литерам. Второй измерение - по дальности. Третье измерение - по азимуту.
		 *
		 * @return: int - указатель на трехмерный массив опорных дальностей для суммарного канала s_
		 */
		eml_32fc*** getS_();

		/**
		 * @brief: Возвращает указатель на трехмерный массив опорных дальностей для разностного канала. Первое измерение - по литерам. Второй измерение - по дальности. Третье измерение - по азимуту.
		 *
		 * @return: int - указатель на трехмерный массив опорных дальностей для разностного канала s_d
		 */
		eml_32fc*** getS_d();
};
#endif /* TKRTSARANGESELECTOR_H */
