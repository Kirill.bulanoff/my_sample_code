#include "TKrtSaRangeSelector.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

TKrtSaRangeSelector::TKrtSaRangeSelector(int MAX_Ndd, int MAX_Nni, int M_ps){
	this->MAX_Ndd = MAX_Ndd;
	this->MAX_Nni = MAX_Nni;
	this->M_ps = M_ps;
	
	s_ = new eml_32fc**[M_ps];
	s_d = new eml_32fc**[M_ps];
	for(int i = 0; i < M_ps; i++){
		s_[i] = eml32fc2dimMalloc(MAX_Ndd, MAX_Nni);
		s_d[i] = eml32fc2dimMalloc(MAX_Ndd, MAX_Nni);
	}	

	SrSum = new eml_32f[MAX_Ndd];
	Sr = new eml_32f[MAX_Ndd];
	analisRange = new bool[MAX_Ndd];
	
	reset();
}


TKrtSaRangeSelector::~TKrtSaRangeSelector(){
	delete[] analisRange;
	delete[] Sr;
	delete[] SrSum;

	for(int i = 0; i < M_ps; i++){
		for(int j = 0; j < MAX_Ndd; j++){
			delete[] s_d[i][j];
			delete[] s_[i][j];
		}
		delete[] s_d[i];
		delete[] s_[i];
	}
	delete[] s_d;
	delete[] s_;
}

void TKrtSaRangeSelector::reset(){
	for(int i = 0; i < M_ps; i++){
		for(int j = 0; j < MAX_Ndd; j++){
			memset(s_[i][j], 0, MAX_Nni * SIZEOF_EML32_FC);
			memset(s_d[i][j], 0, MAX_Nni * SIZEOF_EML32_FC);
		}
	}
	memset(SrSum, 0, MAX_Ndd * SIZEOF_EML32_F);
	memset(Sr, 0, MAX_Ndd * SIZEOF_EML32_F);
	memset(analisRange, 0, MAX_Ndd * sizeof(bool));

	analisRangeCount = 0;
	Sr_m = 0;
}


void TKrtSaRangeSelector::execute(signalItem* start, int Ndd, int Nni, int Ndd_result){
	reset();

	signalItem* p;

	int Nd1 = Ndd / M_ps;
	int Nni_inPK = Nni * M_ps;
	
	for(int i = 0; i < Nd1; ++i){
		p = start;	//Утсанавливаем указатель p на стартовую позицию
		for(int j = 0; j < Nni_inPK; j++){
			SrSum[i] += sqrAbs(p->samples[0][i]);	//Собираем двумерных массив в одномерный, складывая значения на каждой дальности в одно значение
			p = p->next;	//Переход к следующему элементу очереди
		}
	}
	
	//Для сглаживания используется медианный фильтр. Для повышения быстродействия, можно заменить на скользящее среднее 
	eml_Signal_Median_32F(SrSum, Sr, Nd1, 100);

	//Максимальное значение сглаженного массива
	Sr_m = max(Sr, Nd1, NULL);

	for(int i = 0; i < Nd1; ++i){
		analisRange[i] = Sr[i] > 0.5 * Sr_m;	//Присвоить true если элемент массива меньше половины максимума
		if(analisRange[i]){
			analisRangeCount++;	
		}
	}
	while(analisRangeCount > Ndd_result){	//Пока количество отобранных дальностей больше заданного ограничения
		int minIndex = -1;
		for(int i = 0; i < Nd1; i++){
			if(analisRange[i]){
				if(minIndex == -1) minIndex = i;
				if(Sr[minIndex] > Sr[i]){
					minIndex = i;	//Находим минимальый элемент из отобранных
				}
			}
		}
		analisRange[minIndex] = 0;	//Выбрасываем дальность из отобранного массива
		analisRangeCount--;	
	}


	//Поочередно записываем отобранные дальности из очереди в трехмерные массивы s_ и s_d
	int count = 0;
	for(int i = 0; i < Nd1; ++i){
		p = start;
		if(analisRange[i]){
			for(int j = 0; j < Nni; j++){
				for(int k = 0; k < M_ps; k++){
					s_[k][count][j] = p->samples[0][i];	
					s_d[k][count][j] = p->samples[1][i];
					p = p->next;
				}
			}
			count++;
		}
	}
}

int TKrtSaRangeSelector::getAnalisRangeCount(){
	return analisRangeCount;
}

bool* TKrtSaRangeSelector::getAnalisRange(){
	return analisRange;
}

eml_32fc*** TKrtSaRangeSelector::getS_(){
	return s_;
}

eml_32fc*** TKrtSaRangeSelector::getS_d(){
	return s_d;
}
